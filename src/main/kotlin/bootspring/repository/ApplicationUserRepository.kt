package bootspring.repository

import bootspring.dao.ApplicationUsers
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import javax.transaction.Transactional

interface ApplicationUserRepository: JpaRepository<ApplicationUsers, Int>{
    @Transactional
    @Modifying
    @Query(value = "SELECT * FROM app.app_users WHERE username = :username", nativeQuery = true)
    fun doesUserExists(@Param("username") username: String?): Int
}