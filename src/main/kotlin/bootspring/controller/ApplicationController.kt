package bootspring.controller

import bootspring.dao.ApplicationUsers
import bootspring.security.StringUtils.Companion.HEADER_STRING
import bootspring.security.StringUtils.Companion.TOKEN_PREFIX
import bootspring.security.StringUtils.Companion.generateToken
import bootspring.service.ApplicationService
import com.sun.deploy.net.HttpResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse

@RestController
class ApplicationController{

    @Autowired
    lateinit var applicationService:ApplicationService

    @PostMapping("/users/sign-up")
    fun signUp(@RequestBody appUser: ApplicationUsers, response : HttpServletResponse): ResponseEntity<String>{
//        return if(applicationService.isUserExists(appUser.username) > 0) {
            appUser.password = appUser.password
            applicationService.signUp(appUser)
            response.addHeader(HEADER_STRING, TOKEN_PREFIX.toString() + " " + generateToken(appUser.username))
           return ResponseEntity.ok().body<String>(TOKEN_PREFIX + " " + generateToken(appUser.username))
        /*}else{
            ResponseEntity.ok().body<String>("User already exists")
        }*/
    }
}