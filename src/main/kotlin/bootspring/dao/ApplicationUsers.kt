package bootspring.dao

import javax.persistence.*

@Entity
@Table(name="app_users", catalog = "app")
class ApplicationUsers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long ?=0

    @Column(unique=true)
    var username:String ? =null
        /*get() = username                     // getter
        set(name) { username = name }      // setter*/

    var password: String?=null
        /*get() = password
        set(pass) { password = password }*/
}