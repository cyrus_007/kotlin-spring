package bootspring.security

import bootspring.security.StringUtils.Companion.HEADER_STRING
import bootspring.security.StringUtils.Companion.SECRET
import bootspring.security.StringUtils.Companion.TOKEN_PREFIX
import io.jsonwebtoken.Jwts
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class JWTAuthorizationFilter(authmanager: AuthenticationManager) : BasicAuthenticationFilter(authmanager) {

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        var header = request.getHeader(HEADER_STRING)
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response)
            return
        }
        var authentication = getAuthentication(request)
        SecurityContextHolder.getContext().setAuthentication(authentication)
        chain.doFilter(request, response)
    }

    fun getAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        var token = request.getHeader(HEADER_STRING)
        if(token != null){
           var user = Jwts.parser()
//                    .setSigningKey(SECRET.getBytes())
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody().getSubject();
            return if (user != null) {
                UsernamePasswordAuthenticationToken(user, null, ArrayList())
            } else return null
        }
        return null
    }
}