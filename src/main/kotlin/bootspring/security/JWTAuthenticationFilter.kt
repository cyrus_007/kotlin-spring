package bootspring.security

import bootspring.dao.ApplicationUsers
import bootspring.security.StringUtils.Companion.HEADER_STRING
import bootspring.security.StringUtils.Companion.TOKEN_PREFIX
import bootspring.security.StringUtils.Companion.generateToken
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JWTAuthenticationFilter(authenticationManager: AuthenticationManager) : UsernamePasswordAuthenticationFilter() {

    override fun attemptAuthentication(request: HttpServletRequest?, response: HttpServletResponse?): Authentication {
        val creds = ObjectMapper().readValue(request!!.inputStream, ApplicationUsers::class.java)
        return authenticationManager
                .authenticate(UsernamePasswordAuthenticationToken(creds.username, creds.password, ArrayList()))
    }

    override fun successfulAuthentication(request: HttpServletRequest?,
                                          response: HttpServletResponse?,
                                          chain: FilterChain?,
                                          authResult: Authentication?) {
        val username = (authResult!!.getPrincipal() as User).username
        response!!.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + generateToken(username))

    }
}
