package bootspring.security

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import java.util.*

class StringUtils {

    companion object{
    const val SECRET = "SecretKeyToGenJWTs"
    const val EXPIRATION_TIME: Long = 864000000 // 10 days
    const val TOKEN_PREFIX = "Bearer "
    const val SIGN_UP_URL = "/users/sign-up"
    const val HEADER_STRING = "Authorization"

        fun generateToken(username: String?): String? {
            return Jwts.builder()
                    .setSubject(username)
                    .setExpiration(Date(System.currentTimeMillis() + EXPIRATION_TIME))
                    .signWith(SignatureAlgorithm.HS512, SECRET.toByteArray())
                    .compact()
        }
    }
}