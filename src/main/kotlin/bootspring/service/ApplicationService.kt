package bootspring.service

import bootspring.dao.ApplicationUsers

interface ApplicationService {
    fun signUp(appUser: ApplicationUsers)
    fun isUserExists(username:String?): Int
}