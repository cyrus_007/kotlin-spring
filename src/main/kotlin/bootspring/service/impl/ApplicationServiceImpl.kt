package bootspring.service.impl

import bootspring.dao.ApplicationUsers
import bootspring.repository.ApplicationUserRepository
import bootspring.service.ApplicationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ApplicationServiceImpl : ApplicationService {
    @Autowired
    lateinit var applicationUserRepository: ApplicationUserRepository

    override fun signUp(appUser: ApplicationUsers) {
        applicationUserRepository.save(appUser)
    }

    override fun isUserExists(username: String?): Int {
        return applicationUserRepository.doesUserExists(username)
    }

}