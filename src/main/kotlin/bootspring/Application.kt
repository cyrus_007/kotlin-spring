package bootspring

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@EnableJpaRepositories(basePackages = ["bootspring.repository"])
@SpringBootApplication
open class Application

fun main(args: Array<String>){
    SpringApplication.run(Application::class.java, *args)
//    runApplication<Application>(*args)
}

